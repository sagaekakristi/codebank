from typing import List, Callable, Dict
from shutil import which

import argparse
import pdb
import subprocess
import logging
import os

assert callable(pdb.set_trace)

class FilesystemUtility:
    @staticmethod
    def is_folder_exist(
        folderpath: str,
    ) -> bool:
        return os.path.isdir(folderpath)

    @staticmethod
    def is_file_exist(
        filepath: str,
    ) -> bool:
        return os.path.isfile(filepath)

    @staticmethod
    def create_folder_if_not_exist(
        folderpath: str,
    ):
        os.makedirs(folderpath, exist_ok=True)

    @staticmethod
    def get_file_content(
        filepath: str,
    ) -> str:
        with open(filepath, 'r') as file:
            content: str = file.read()
        return content

    @staticmethod
    def write_file_content(
        filepath: str,
        content: str,
    ):
        with open(filepath, 'w') as file:
            file.write(content)

class ShellUtility:
    @staticmethod
    def execute(cmd:str, allow_fail:bool = False, print_stdout:bool = True):
        logging.info("executing: {}".format(cmd))
        proc = subprocess.Popen(
            cmd,
            shell=True,
            stdout = subprocess.PIPE,
            stderr = subprocess.STDOUT,
        )

        if print_stdout:
            for line in iter(proc.stdout.readline, b''):
                print(line.rstrip().decode())

        stdout, stderr = proc.communicate()
        if proc.returncode == 0:
            success = True
        else:
            success = False

        if (not success) and (not allow_fail):
            if print_stdout == False:
                raise Exception("got non-zero exit code {} when executing: '{}' | stdout: {} | stderr: {}".format(
                    str(proc.returncode),
                    cmd,
                    stdout.decode() if stdout != None else stdout,
                    stderr.decode() if stderr != None else stderr,
                ))
            else:
                raise Exception("got non-zero exit code {} when executing: '{}'".format(
                    str(proc.returncode),
                    cmd,
                ))

        return dict(
            success = success,
            proc = proc,
            stdout = stdout.decode() if stdout != None else stdout, 
            stderr = stderr.decode() if stderr != None else stderr,
        )

class Preparator:
    def prepare_gcloud(**kwargs):
        if not (which('gcloud') == None):
            print('gcloud already installed. skip installation.')
            return

        # do
        command: str = 'cd /opt && curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-cli-390.0.0-linux-x86_64.tar.gz && tar -xf google-cloud-cli-390.0.0-linux-x86_64.tar.gz && rm google-cloud-cli-390.0.0-linux-x86_64.tar.gz && ./google-cloud-sdk/install.sh --quiet && ln -s /opt/google-cloud-sdk/bin/* /usr/local/bin/'
        print(f"executing: {command}")
        ShellUtility.execute(
            cmd = command,
            allow_fail = False,
            print_stdout = True,
        )

        # assert
        assert which('gcloud') != None
        assert which('gsutil') != None

    def prepare_activate_gcp_sa(**kwargs):
        assert 'GOOGLE_APPLICATION_CREDENTIALS' in os.environ
        assert FilesystemUtility.is_file_exist(
            filepath = os.environ['GOOGLE_APPLICATION_CREDENTIALS'],
        )

        command: str = "gcloud auth activate-service-account --key-file={gcp_sa_path}".format(
            gcp_sa_path = os.environ['GOOGLE_APPLICATION_CREDENTIALS'],
        )
        print(f"executing: {command}")
        ShellUtility.execute(
            cmd = command,
            allow_fail = False,
            print_stdout = True,
        )

    def prepare_pull_code_grom_gcs(**kwargs):
        assert which('gcloud') != None
        assert which('gsutil') != None
        assert kwargs['args']['workspace_path'] != None
        assert kwargs['args']['code_gcs_bucket'] != None
        assert kwargs['args']['code_gcs_folder'] != None

        # do
        FilesystemUtility.create_folder_if_not_exist(
            folderpath = kwargs['args']['workspace_path'],
        )
        command: str = "gsutil -m rsync -r -c -d gs://{code_gcs_bucket}/{code_gcs_folder} file://{workspace_path}".format(
            workspace_path = kwargs['args']['workspace_path'],
            code_gcs_bucket = kwargs['args']['code_gcs_bucket'],
            code_gcs_folder = kwargs['args']['code_gcs_folder'],
        )
        print(f"executing: {command}")
        ShellUtility.execute(
            cmd = command,
            allow_fail = False,
            print_stdout = True,
        )

        # assert
        assert FilesystemUtility.is_folder_exist(folderpath = kwargs['args']['workspace_path'])

    def prepare_install_packages(**kwargs):
        assert kwargs['args']['workspace_path'] != None
        assert which('pip') != None

        workspace_path: str = kwargs['args']['workspace_path']
        assert os.path.isfile(f"{workspace_path}/requirements/pip-requirements.txt")

        # do
        command: str = "pip install -r {workspace_path}/requirements/pip-requirements.txt".format(
            workspace_path = workspace_path,
        )
        print(f"executing: {command}")
        ShellUtility.execute(
            cmd = command,
            allow_fail = False,
            print_stdout = True,
        )

    def prepare_uninstall_packages(**kwargs):
        assert kwargs['args']['packages_to_uninstall'] != None
        assert which('pip') != None

        packages_to_uninstall: List[str] = kwargs['args']['packages_to_uninstall'].split(',')
        for package in packages_to_uninstall:
            # do
            command: str = "pip uninstall -y {package}".format(
                package = package,
            )
            print(f"executing: {command}")
            ShellUtility.execute(
                cmd = command,
                allow_fail = False,
                print_stdout = True,
            )

def do_prepare(args: Dict):
    components: List[str] = args['components'].split(',')
    print(f"components to execute: {components}")

    for component in components:
        preparator_function: Callable = getattr(Preparator, f"prepare_{component}")
        # execute
        preparator_function(
            args = args,
            component = component,
        )

    print(f"done preparing environment with components: {components}")

def main():
    parser = argparse.ArgumentParser(
        prog = 'Prepare',
        description = 'Prepare environment of notebook execution',
        # epilog = '',
    )
    parser.add_argument(
        '--components', type = str, dest = 'components', default = 'gcloud,activate_gcp_sa,pull_code_grom_gcs,install_packages,uninstall_packages',
        help = 'comma-separated values of components that will be prepared, e.g. gcloud,pull_code'
    )
    parser.add_argument(
        '--workspace_path', type = str, dest = 'workspace_path', default = '/workspace/workground',
        help = 'path to folder where code will be pulled, e.g. /workspace'
    )
    parser.add_argument(
        '--code_gcs_bucket', type = str, dest = 'code_gcs_bucket', default = 'saga-neuralic-code',
        help = 'name of GCS bucket where code will be pulled from, e.g. saga-neuralic-code'
    )
    parser.add_argument(
        '--code_gcs_folder', type = str, dest = 'code_gcs_folder', default = 'neuralic',
        help = 'path to GCS folder where code will be pulled from, e.g. path/to/neuralic'
    )
    parser.add_argument(
        '--packages_to_uninstall', type = str, dest = 'packages_to_uninstall', default = 'transformer-engine',
        help = 'comma-separated values of pip packages to uninstall, e.g. transformer-engine'
    )
    args_raw = parser.parse_args()
    args: Dict = vars(args_raw)

    do_prepare(args = args)
    # pdb.set_trace()

if __name__ == '__main__': main()
